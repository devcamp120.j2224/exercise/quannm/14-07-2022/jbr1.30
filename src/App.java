public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        Employee employee1 = new Employee(1, "Nguyen", "Quan", 15000000);
        Employee employee2 = new Employee(2, "Ho", "Boi", 12000000);

        System.out.println(employee1 + ", " + employee2);

    }
}
