public class Employee {
    int id;
    String firstName;
    String lastName;
    int salary;

    public Employee(int id, String firstName, String lastName, int salary) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.salary = salary;
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public String getName() {
        return getFirstName() + " " + getLastName();
    }

    public int getAnnualSalary() {
        return getSalary() * 12;
    }

    public int raiseSalary() {
        return (int) (salary * 1.1);
    }

    @Override
    public String toString() {
        return "Employee[id=" + getId() + ", name=" + getName() + ", salary=" + getAnnualSalary() + ", raiseSalary=" + raiseSalary() + "]";
    }
}
